<?php

/**
 * @file
 * Contains \Drupal\block_views\Plugin\Condition\ViewPage.
 */

namespace Drupal\block_views\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'View Page' condition.
 * @todo: context entity:view
 *
 * @Condition(
 *   id = "view_page",
 *   label = @Translation("View Pages")
 * )
 *
 */
class ViewPage extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Creates a new ViewPage instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityStorageInterface $entity_storage, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('view'), $configuration, $plugin_id, $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array('view_pages' => array()) + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = array();
    foreach ($this->entityStorage->loadMultiple() as $view) {
      /* @var $view \Drupal\views\Entity\View */
      foreach ($view->get('display') as $display_id => $display) {
        if ($display_id == 'default') {
          continue;
        }
        if ($display['display_plugin'] != 'page') {
          continue;
        }
        $options[$view->id() . ':' . $display_id] = $view->label() . ' - ' . $display['display_title'];
      }
    }
    $default_values = array();
    foreach ($this->configuration['view_pages'] as $view_id => $displays) {
      foreach ($displays as $display_id) {
        $default_values[] = $view_id . ':' . $display_id;
      }
    }
    $form['view_pages'] = array(
      '#title' => $this->t('View pages'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $default_values
    );
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['view_pages'] = array();
    foreach (array_keys(array_filter($form_state->getValue('view_pages'))) as $view) {
      $view = explode(':', $view);
      if (count($view) > 1) {
        $this->configuration['view_pages'][$view[0]][] = $view[1];
      }
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return 'summary';
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['view_pages']) && !$this->isNegated()) {
      return TRUE;
    }
    /* @todo: context entity:view */
    $route_object = \Drupal::routeMatch()->getRouteObject();
    if (!($route_object->hasDefault('_controller') && $route_object->hasDefault('view_id') && $route_object->hasDefault('display_id') && $route_object->getDefault('_controller') == 'Drupal\views\Routing\ViewPageController::handle')) {
      return false;
    }
    $view_id = $route_object->getDefault('view_id');
    $display_id = $route_object->getDefault('display_id');
    return (array_key_exists($view_id, $this->configuration['view_pages']) && array_search($display_id, $this->configuration['view_pages'][$view_id]) !== false);
  }

}
